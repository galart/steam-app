const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const PromiseThrottle = require('promise-throttle');

const app = express();
const port = process.env.PORT || 5000;
const steam_api_private_key = '7D5F2FA02FF09ACA687DE979BE355B30';
const stream_api_get_owned_games = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/';
const stream_api_get_steamId = 'http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/';

app.use(bodyParser.json());

const throttleOptions = {
  requestsPerSecond: 4,
  promiseImplementation: Promise
};

const throttle = new PromiseThrottle(throttleOptions);

app.post('/api/games', (req, res, next) => {
  let steamUrls = req.query.steamUrls, _p;

  let steamIds = steamUrls.reduce(function(acc, curr) {
    let urlArr = curr.split('/').filter(v=>v!='');
    if (urlArr[urlArr.length - 2] === 'id') {
      acc.incorrect.push(urlArr[urlArr.length - 1]);
    } else if (urlArr[urlArr.length - 2] === 'profiles') {
      acc.correct.push(urlArr[urlArr.length - 1]);
    }
    return acc;
  }, {correct: [], incorrect: []});

  if (steamIds.incorrect.length) {
    _p = Promise.all(steamIds.incorrect.map((nickname) => {
      return axios.get(stream_api_get_steamId, {
        params: {
          key: steam_api_private_key,
          vanityurl: nickname
        }
      })
    })).then(function(data){
      if (!data.every((d) => d.data.response.success === 1)) {
        return Promise.reject(createError({
          message: 'profile not found',
          code: 400
        }));
      }
      data.forEach((d) => steamIds.correct.push(d.data.response.steamid));
      return steamIds.correct;
    }, function(e){ return createError(e); });
  } else {
    _p = Promise.resolve(steamIds.correct);
  }

  _p.then(function(correctSteamIds) {
    Promise.all(correctSteamIds.map(function(id){
      return axios.get(stream_api_get_owned_games, {
        params: {
          format: 'json',
          key: steam_api_private_key,
          steamid: id,
          include_appinfo: 1
        }
      })
    })).then(function(data){
      if (!data.every((d) => d.data.response.games)) {
        next(createError({
          message: 'profile is closed',
          code: 400
        }));
      } else {
        res.send(data.map(function(d) {
          return d.data.response.games
        }));
      }
    }, function(e) { next(createError(e)); });
  }, function(e){ next(e); });
});

const getMultiPlayerGames = function(req, res, next) {
  return axios.get('http://steamspy.com/api.php', {
    params: {
      request: 'tag',
      tag: 'multiplayer'
    }
  }).then(function(d) {
      if (Object.keys(d.data).length === 0) {
        next(createError({
          message: 'steamspy is fail',
          code: 400
        }));
      } else {
        res.send(Object.keys(d.data).map(Number));
      }
    },
    function(e){ next(createError(e)); }
  )
};

app.get('/api/multiplayer', (req, res, next) => {
  return throttle.add(getMultiPlayerGames.bind(this, req, res, next));
});

app.use(function(err, req, res, next) {
  res.status(err.httpStatusCode || 400).send({
    message: err.message,
    code: err.httpStatusCode || 400
  });
});

function createError(e) {
  let error = new Error(e.message);
  error.httpStatusCode = e.code;
  return error;
}

app.listen(port, () => console.log(`Listening on port ${port}`));

