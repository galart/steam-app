import React, { Component } from 'react';
import styles from './App.css';
import logo from '../logo.png';
import { inject, observer } from 'mobx-react';
import Preloader from './Preloader';
import SearchForm from  './SearchForm';

const steam_base_game_url = 'https://store.steampowered.com/app';

const App = inject('commonStore')(observer(
  class App extends Component {

    renderGamesList(){
      return <div className={styles.gamesList}>
        {this.props.commonStore.multiPlayerGames.map((game, i)=>{
          return <div key={i} className={styles.gameItem}>
            <a href={`${steam_base_game_url}/${game.appid}`}><img src={game.logo}/></a>
          </div>
        })}
      </div>
    }

    render() {
      const { loading, form, onFieldChange, onSubmit } = this.props.commonStore;
      return (
        <div className={styles.app}>
          <header className={styles.appHeader}>
            <img src={logo} alt="logo"/> <span>matcher</span>
          </header>
          <div className={styles.wrapper}>
            <SearchForm
              form={form}
              onChange={onFieldChange}
              onSubmit={onSubmit}
              loading={loading}
            />
            {loading ? <Preloader></Preloader> : this.renderGamesList()}
          </div>
        </div>
      );
    }
  }
));

export default App;
