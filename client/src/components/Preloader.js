import React from 'react';
import styles from './Preloader.css';

const Preloader = () => <div className={styles.preloader}></div>;
export default Preloader;
