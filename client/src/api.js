import axios from 'axios';
import queryString from 'query-string';

export class APIRequest {
  constructor(method = 'get', url) {
    this.method = method;
    this.url = url;
  }

  exec(params) {
    let url = this.prepareUrl(this.url, params);

    return axios[this.method](url)
      .then((response) => {
        return response.data;
      }).catch((error) => {
        throw error.response.data;
      });
  }

  prepareUrl(url, params){
    if (params) {
      return url + '?' + queryString.stringify(params)
    }
    return url;
  }
}

export function apiGET(url) {
  let req = new APIRequest('get', url);

  return (...argv) => req.exec(...argv);
}

export function apiPOST(url, options) {
  let req = new APIRequest('post', url, options);

  return (...argv) => req.exec(...argv);
}

