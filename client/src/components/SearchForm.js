import React from 'react';
import PropTypes from "prop-types";
import { observer } from 'mobx-react'
import styles from './SearchForm.css';

const SearchForm = observer((props) => {

    const { form, onChange, onSubmit, loading } = props;
    const { fields, meta } = form;

    return (
      <form>
        <div className={styles.inputField}>
          <input
            className={styles.mainInput}
            onChange={(e) => onChange(e.target.name, e.target.value)}
            type="text"
            name="accountsUrls"
            value={fields.accountsUrls.value}
            placeholder="Впишите две или более ссылки на steam аккаунты разделенные пробелом"
          />
          <button
            className={styles.searchButton}
            onClick={onSubmit}
            type="button"
            disabled={loading || !meta.isValid}
          >Искать</button>
        </div>
        {fields.accountsUrls.error && <div className={styles.inputFieldError}>{fields.accountsUrls.error}</div>}
        {meta.error && <div className={styles.inputFieldError}>{meta.error}</div>}
      </form>
    )
  }
);

SearchForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  form: PropTypes.object.isRequired
};

export default SearchForm;
