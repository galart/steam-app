import React from 'react';
import { Provider } from 'mobx-react';
import App from './components/App';

import commonStore from './stores/commonStore';

const stores = {
  commonStore
};

class Root extends React.Component {
  render() {
    return (
      <Provider {...stores}>
        <App/>
      </Provider>
    );
  }
}

export default Root;
