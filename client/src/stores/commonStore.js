import { observable, action, computed, decorate, toJS  } from 'mobx';
import Validator from 'validatorjs';
import { apiGET, apiPOST } from '../api';
import map from 'lodash/map';
import compact from 'lodash/compact';
import intersection from 'lodash/intersection';

const steam_profile_url_regex = /https?:\/\/steamcommunity\.com\/(?:profiles|id)\/[a-zA-Z0-9]+/;
const steamLogoGamesBaseUrl = 'http://media.steampowered.com/steamcommunity/public/images/apps';
const apiMethods = {
  getAllMultiPlayerGames: apiGET('/api/multiplayer'),
  getGames: apiPOST('/api/games')
};

class CommonStore {

  constructor(){
    Validator.useLang('ru');
    Validator.register('twoOrMoreLinksRequired', function(value) {
      return compact(value.trim().split(' ')).length >= 2
    }, 'Требуется вввести 2 или более ссылки');
    Validator.register('linkMustBeSteamProfileUrl', function(value) {
      return compact(value.trim().split(' ')).every((urlString) => {
        return steam_profile_url_regex.test(urlString);
      });
    }, 'Неверный формат');
  }

  loading = false;
  sharedGames;
  sharedMultiPlayerGames = [];
  allPlayersGames;

  form = {
    fields: {
      accountsUrls: {
        value: '',
        error: null,
        rule: 'required|twoOrMoreLinksRequired|linkMustBeSteamProfileUrl'
      },
    },
    meta: {
      isValid: false,
      error: null,
    }
  };

  getFlattenedValues = (valueKey = 'value') => {
    let data = {};
    let form = toJS(this.form).fields;
    Object.keys(form).map(key => {
      data[key] = form[key][valueKey]
    });
    return data
  };

  onFieldChange = (field, value) => {
    this.form.meta.error = null;
    this.form.fields[field].value = value;
    let validation = new Validator(
      this.getFlattenedValues('value'),
      this.getFlattenedValues('rule'));
    this.form.meta.isValid = validation.passes();
    this.form.fields[field].error = validation.errors.first(field)
  };

  onSubmit = () => {
    this.pullUserGames();
  };

  get multiPlayerGames() {
    if (!this.sharedMultiPlayerGames) {
      return false;
    }
    return this.sharedMultiPlayerGames.map((game) => {
      return this.allPlayersGames[game];
    })
  };

  pullUserGames() {
    const { accountsUrls } = this.form.fields;

    this.loading = true;
    return apiMethods.getGames({steamUrls: accountsUrls.value.split(' ')})
    .then(action((data) => {
      this.allPlayersGames = data.reduce((list, currGamesSet) => {
        return currGamesSet.reduce((acc, curr) => {
            acc[curr.appid] = {
              logo: `${steamLogoGamesBaseUrl}/${curr.appid}/${curr.img_logo_url}.jpg`,
              appid: curr.appid
            };
            return acc;
        }, list)
      }, {});

      this.sharedGames = intersection(...data.map((gamesEntity)=>map(gamesEntity, 'appid')));
      return apiMethods.getAllMultiPlayerGames()
    }), action((error) => {
      if(error.code === 400 && error.message === 'profile not found') {
        this.form.meta.error = 'Один или более профилей не существуют';
      } else if (error.code === 400 && error.message === 'profile is closed') {
        this.form.meta.error = 'Один или более профилей закрыты от публичного просмотра';
      }
    }))
    .then(action((data) => {
      this.sharedMultiPlayerGames = intersection(this.sharedGames, data);
    }), (error) => {
      if (error.code === 400 && error.message === 'steamspy is fail') {
        this.form.meta.error = 'Steamspy отдает пустой обьект';
      }
    })
    .finally(action(() => { this.loading = false; }))
  }
}

decorate(CommonStore, {
    loading: observable,
    sharedMultiPlayerGames: observable,
    form: observable,
    pullUserGames: action,
    onFieldChange: action,
    multiPlayerGames: computed
  }
);

export default new CommonStore();




